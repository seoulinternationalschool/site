const path = require('path');

module.exports = {
  port: 3002,
  prefix: '/',
  DATABASE: {
    host: 'localhost',
    user: 'blog',
    password: 'whatyoulookinfor',
    database: 'bloggerino',
    port: 3306,
  },
  SASS: {
    src: path.join(__dirname, 'sass'),
    dest: path.join(__dirname),
    force: true,
    outputStyle: 'expanded',
    prefix: '/',
  },
};

