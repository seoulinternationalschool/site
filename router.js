const path = require('path');
const express = require('express');
const sassMiddleware = require('node-sass-middleware');
const debug = require('debug')('app:site');
const moment = require('moment');
const config = require('./config');

debug('initializing');

// Setup Router
const app = express();
app.set('view engine', 'pug');
app.set('views', `${__dirname}/views`);

app.use(require('./controllers/index'));
app.use(require('./controllers/blog'));
app.use(require('./controllers/projects'));

app.use(sassMiddleware({
  src: path.join(__dirname, 'sass'),
  dest: path.join(__dirname),
  force: true,
  outputStyle: 'compressed',
  prefix: '/',
}));
app.use('/public', express.static(`${__dirname}/public`));
app.get('*', (req, res) => {
  res.status(404).render('error.pug');
});

module.exports = app;
