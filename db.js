const debug = require('debug')('app:site:db');
const mysql = require('mysql');

const config = require('./config');

debug('creating DB pool');
module.exports = mysql.createPool(config.DATABASE);
