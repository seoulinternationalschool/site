document.addEventListener('DOMContentLoaded', () => {
  const $burger = document.getElementById('nav-burger');
  const $menu = document.getElementById('nav-menu');

  $burger.addEventListener('click', () => {
    if ($menu.classList.contains('is-active')) {
      $menu.classList.remove('is-active');
      $burger.classList.remove('is-active');
    } else {
      $menu.classList.add('is-active');
      $burger.classList.add('is-active');
    }
  });
});
