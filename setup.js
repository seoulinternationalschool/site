const mysql = require('mysql');
const readline = require('readline');
const config = require('./config');

const con = mysql.createConnection({
  host: config.DATABASE.host,
  user: config.DATABASE.user,
  port: config.DATABASE.port,
  password: config.DATABASE.password,
});

console.log(`Establishing connection with user ${config.DATABASE.user}...`);
con.connect((err) => {
  // check mysql server
  if (err) {
    console.log('ERR: connecting to server failed.');
    throw err;
    process.exit(-1);
  }
  console.log('OK: mysql server connected');

  checkDatabase();
});


function checkDatabase(callback) {
  con.query('CREATE DATABASE IF NOT EXISTS ??', config.DATABASE.database, (err) => {
    if (err) throw err;
    console.log('OK: database exists');
    useDatabase();
  });
}

function useDatabase() {
  con.changeUser({ database: config.DATABASE.database }, (err) => {
    if (err) throw err;
    console.log('OK: connected to database');
    checkPostsTable();
  });
}

function checkPostsTable() {
  con.query(`CREATE TABLE IF NOT EXISTS posts (
    ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Title varchar(100),
    Author varchar(30),
    Content text,
    Timestamp datetime
  )`, (err) => {
    if (err) throw err;
    console.log('OK: successfully created posts table');
    checkProjectsTable();
  });
}

function checkProjectsTable() {
  con.query(`CREATE TABLE IF NOT EXISTS projects (
    ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Name varchar(100),
    Description varchar(1000),
    Maintainers text,
    Status_Percent int(11),
    Status_Desc varchar(100),
    Site varchar(100),
    Git varchar(120),
    Version varchar(120)
  )`, (err, results) => {
    if (err) throw err;
    console.log('OK: successfully created projects table');
    terminate();
  });
}

function terminate() {
  con.end();
}
