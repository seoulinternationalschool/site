const debug = require('debug')('app:site:blog');
const express = require('express');
const moment = require('moment');
const db = require('../db.js');

const router = express.Router();

router.get('/blog', (req, res) => {
  const PAGINATION = 10;
  const queryPageNum = req.query.page === undefined ? 0 : req.query.page;
  const paginationStart = PAGINATION * queryPageNum;

  db.query('SELECT * FROM posts ORDER BY Timestamp DESC LIMIT ?, 10', paginationStart, (err, result) => {
    if (err) debug('error fetching information from blogs');
    res.render('blog_root', {
      posts: result,
      moment,
    });
  });
});

router.get('/blog/:blogid', (req, res, next) => {
  db.query('SELECT * FROM posts WHERE ID=?', req.params.blogid, (err, result) => {
    if (err) debug('error fetching information from blogs');
    if (result.length === 0) next();
    res.render('blog', {
      post: result[0],
      moment,
    });
  });
});

module.exports = router;
