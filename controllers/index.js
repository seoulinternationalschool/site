const debug = require('debug')('app:site:index');
const express = require('express');
const moment = require('moment');
const db = require('../db.js');

const router = express.Router();

router.get('/', (req, res) => {
  db.query('SELECT * FROM posts ORDER BY Timestamp DESC LIMIT 10', (err, result) => {
    res.render('index', {
      posts: result,
      moment,
    });
  });
});

module.exports = router;
