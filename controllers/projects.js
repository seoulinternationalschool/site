const debug = require('debug')('app:site:projects');
const express = require('express');
const moment = require('moment');
const db = require('../db.js');

const router = express.Router();

router.get('/projects', (req, res) => {
  db.query('SELECT * FROM projects', (err, result) => {
    if (err) debug('error fetching data from projects table');
    res.render('projects', {
      projects: result,
    });
  });
});

module.exports = router;
